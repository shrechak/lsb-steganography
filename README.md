Informations
--------------

keyGen.py - Used to generate the private,public keys as required by RSA , and the OAuth key as required by user validation

LSBSteg.py - Used to embed the required information in the image.

imageSeg.py - Used to divide the image into coarse and fine signals(to be implemented)

appModule.py - Used to perform the actual bank and customer use cases

gui.py - Used to make the apptication into a single stand alone software(to be completed)

How to use it ?
---------------
