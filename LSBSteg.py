import cv2.cv as cv
import sys

class SteganographyException(Exception):
    pass

class LSBSteg():
    def __init__(self, im):
        self.image = im
        self.width = im.width
        self.height = im.height
        self.size = self.width * self.height
        self.nbchannels = im.channels
        
        self.maskONEValues = [1,2,4,8,16,32,64,128]
        #Mask used to put one ex:1->00000001, 2->00000010 .. associated with OR bitwise
        self.maskONE = self.maskONEValues.pop(0) 
        
        self.maskZEROValues = [254,253,251,247,239,223,191,127]
        #Mask used to put zero ex:254->11111110, 253->11111101 .. associated with AND bitwise
        self.maskZERO = self.maskZEROValues.pop(0)
        
        self.curwidth = 0 
        self.curheight = 0 
        self.curchan = 0 
     
    def saveImage(self,filename):
        cv.SaveImage(filename, self.image)
                    

    def putBinaryValue(self, bits): 
        for c in bits:
            val = list(self.image[self.curwidth,self.curheight]) #Get the pixel value as a list
            if int(c) == 1:
                val[self.curchan] = int(val[self.curchan]) | self.maskONE #OR with maskONE
            else:
                val[self.curchan] = int(val[self.curchan]) & self.maskZERO #AND with maskZERO
                
            self.image[self.curwidth,self.curheight] = tuple(val)
            self.splitCoarseFine()

    def splitCoarseFine(self):
	self.nextSpace() 
	self.nextSpace()

    def imgSegment(self,bits):
	pixels = []
	counts = []
        for i in range(self.width):
	    for j in range (self.height):
		pixels.append(list(self.image[i,j]))
	for p in pixels:
	    counts.append(pixels.count(p))
	threshold = max(count) * 0.7
	for i in range(len(counts)):	
            if count[i]<threshold:
		self.nextSpace()
	    else:
		self.putBinaryValue(bits) 

    def nextSpace(self):
        if self.curchan == self.nbchannels-1: 
            self.curchan = 0
            if self.curwidth == self.width-1: 
                self.curwidth = 0
                if self.curheight == self.height-1:
                    self.curheight = 0
                    if self.maskONE == 128: 
                        raise SteganographyException, "Image filled"
                    else: 
                        self.maskONE = self.maskONEValues.pop(0)
                        self.maskZERO = self.maskZEROValues.pop(0)
                else:
                    self.curheight +=1
            else:
                self.curwidth +=1
        else:
            self.curchan +=1

    def readBit(self): 
        val = self.image[self.curwidth,self.curheight][self.curchan]
        val = int(val) & self.maskONE
        self.splitCoarseFine()
        if val > 0:
            return "1"
        else:
            return "0"
    
    def readByte(self):
        return self.readBits(8)
    
    def readBits(self, nb): 
        bits = ""
        for i in range(nb):
            bits += self.readBit()
        return bits

    def byteValue(self, val):
        return self.binValue(val, 8)
        
    def binValue(self, val, bitsize): 
        binval = bin(val)[2:]
        if len(binval) > bitsize:
            raise SteganographyException, "binary value larger than the expected size"
        while len(binval) < bitsize:
            binval = "0"+binval
        return binval

    def hideText(self, txt):
        l = len(txt)
        binl = self.binValue(l, 16)
        self.putBinaryValue(binl)
        for char in txt: 
            c = ord(char)
            self.putBinaryValue(self.byteValue(c))
       
    def unhideText(self):
        ls = self.readBits(16) 
        l = int(ls,2)
        i = 0
        unhideTxt = ""
        while i < l: 
            tmp = self.readByte() 
            i += 1
            unhideTxt += chr(int(tmp,2)) 
        return unhideTxt
    
if __name__=="__main__":
    pass
