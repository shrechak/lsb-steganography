from flask import render_template, flash, redirect, request, make_response
from app import app
from .forms import RegistrationForm
import keyGen as key,execute
import Image,appModule as am
from pymongo import MongoClient
import random,string,execute

client = MongoClient()
db = client['netbankingusers']
session = {}

@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'Working'}  # fake user
    return render_template('index.html',
                           title='Home',
                           user=user)

@app.route('/register', methods=['GET','POST'])
def register():
    form = RegistrationForm()
    return render_template('register.html', 
                           title='Register',
                           form=form)

@app.route('/download')
def download():
    name = request.args.get('name')
    accno = request.args.get('account_no')
    user_id = request.args.get('user_id')
    key.registerUser(name,accno,user_id)
    am.hideCustomerImage('name.jpg',user_id)
    oauthkey=db.oauth.find_one({"user_id" : user_id})['oauth']
    execute.genCode(user_id,str(oauthkey))
    return render_template('download.html', 
                           title='Download')

@app.route('/bankside', methods=['GET', 'POST'])
def bankside_generate():
    form = RegistrationForm()
    return render_template('bankSide.html', 
                           title='bankside',
                           form=form)

@app.route('/getBankImage')
def getBankImage():
    form = RegistrationForm()
    user = request.args.get('userid')
    chars = "".join( [random.choice(string.letters) for i in xrange(6)] )
    session['message'] = chars
    am.hideBankImage(chars,'name.jpg',user)
    return render_template('validatePage.html', 
                           title='validation',
                           form=form)

@app.route('/validate')
def validateUser():
    form = RegistrationForm()
    message = request.args.get('message')
    if session['message'] == message :
      return "ok!"

@app.route('/img/<path:filename>')
def send_img(filename):
    response = make_response(app.static_folder+"/"+filename)
    response.headers['Content-Type'] = 'image/png'
    response.headers['Content-Disposition'] = 'attachment; filename='+filename
    return response

@app.route('/script/<path:filename>')
def send_script(filename):
    response = make_response(app.static_folder+"/"+filename)
    response.headers['Content-Type'] = 'text'
    response.headers['Content-Disposition'] = 'attachment; filename='+filename
    return response
