from flask.ext.wtf import Form
from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired

class RegistrationForm(Form):
    name = StringField('AccountHolderName', validators=[DataRequired()])
    account_no = StringField('AccountNo', default="xxxxxxxxxx")
