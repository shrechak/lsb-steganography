from LSBSteg import LSBSteg
import cv,keyGen
from pymongo import MongoClient

obj=keyGen.RSAkey()
client = MongoClient()
db = client['netbankingusers']

def chanValue(imageName):
	im = cv.LoadImage(imageName)
	steg = LSBSteg(im)
	return steg.nbchannels


# def encr(text,user_id):
# 	

# def decr(crypt,user_id):
# 	

def hideBankImage(textToHide,imageName,user_id):
	carrier = cv.LoadImage(imageName)
	steg = LSBSteg(carrier)
	privateKey_d=long(db.cryptkeys.find_one({"user_id" : user_id})['priv_d'])
	privateKey_p=long(db.cryptkeys.find_one({"user_id" : user_id})['priv_p'])
	privateKey_q=long(db.cryptkeys.find_one({"user_id" : user_id})['priv_q'])
	publicKey_n=long(db.cryptkeys.find_one({"user_id" : user_id})['pub_n'])
	publicKey_e=long(db.cryptkeys.find_one({"user_id" : user_id})['pub_e'])
	key=obj.createKey((publicKey_n,publicKey_e,privateKey_d,privateKey_p,privateKey_q))
	crypto=key.encrypt(textToHide,'x')[0]
	steg.curchan=0
	steg.hideText(crypto)
	steg.curchan=2	
	steg.hideText(str(publicKey_n))	
	steg.curchan=1
	steg.hideText(str(publicKey_e))
	steg.saveImage("app/static/bankResult.png")

def unhideBankImage(imageName,key):
	im = cv.LoadImage(imageName)
	steg = LSBSteg(im)
	steg.curchan=0
	text=steg.unhideText()
	steg.curchan=2
	n=long(steg.unhideText())
	steg.curchan=1
	e=long(steg.unhideText())
	k1=(n,e)
	finalKey=k1+key
	return obj.createKey(finalKey).decrypt(text)

def hideCustomerImage(imageName,user_id):
	carrier = cv.LoadImage(imageName)
	steg = LSBSteg(carrier)
	privateKey_d=db.cryptkeys.find_one({"user_id" : user_id})['priv_d']
	steg.curchan=0
	steg.hideText(privateKey_d)
	privateKey_p=db.cryptkeys.find_one({"user_id" : user_id})['priv_p']
	steg.curchan=1
	steg.hideText(privateKey_p)
	privateKey_q=db.cryptkeys.find_one({"user_id" : user_id})['priv_q']
	steg.curchan=2
	steg.hideText(privateKey_q)
	steg.saveImage("app/static/custResult.png")

def unhideCustomerImage(imageName):
	im = cv.LoadImage(imageName)
	steg = LSBSteg(im)
	steg.curchan=0
	d=long(steg.unhideText())
	steg.curchan=1
	p=long(steg.unhideText())
	steg.curchan=2
	q=long(steg.unhideText())
	return (d,p,q)

def getMessage(personal,downloaded):
	tup=unhideCustomerImage(personal)
	msg=unhideBankImage(downloaded,tup)
	return msg

def getMsg(cypto,personal,downloaded):
	tup=unhideCustomerImage(personal)
	key=unhideBankImage(downloaded,tup)
	return key.decrypt(cypto)



