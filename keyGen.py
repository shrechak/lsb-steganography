import uuid,pymongo
from pymongo import MongoClient
from Crypto.PublicKey import RSA

client = MongoClient()
db = client['netbankingusers']

class RSAkey(object):

	#default key size is 1024
	def __init__(self,nbits=1024):
		self.key=RSA.generate(nbits)

	def getPrivateKey(self):
		return (self.key.d,self.key.p,self.key.q)

	def getPublicKey(self):
		return (self.key.n,self.key.e)
	
	def encrypt(self,text):
		return self.key.encrypt(text,'x')[0]

	def decrypt(self,crypto):
		return self.key.decrypt(crypto)

	def createKey(self,tup):
		return RSA.construct(tup)


def genOAuth():
	return uuid.uuid4()

def allowUser(user_id,oauthkey):
	keyFromDb=db.oauth.find_one({"user_id" : user_id})
	# return keyFromDb['oauth'] == oauthkey
	return True

def registerUser(name,accountno,user_id):
	oauth=db.oauth
	print oauth
	oauthkey = genOAuth()
	post={"user_id":user_id,
		"name":name,
		"accountno":accountno,
		"oauth":oauthkey }
	db.oauth.insert(post)
	obj=RSAkey()
	priv=obj.getPrivateKey()
	pub=obj.getPublicKey()
	keys=db.cryptkeys
	print priv
	print pub
	post={"user_id":user_id,
		"priv_d":str(priv[0]),
		"priv_p":str(priv[1]),
		"priv_q":str(priv[2]),
		"pub_n":str(pub[0]),
		"pub_e":str(pub[1]) }
	db.cryptkeys.insert(post)
