from setuptools import setup

setup(name='netBankingSteg',
      version='0.1',
      description='An alternative to net banking',
      license='SRM',
      packages=['netBankingSteg'],
      install_requires=[
            'numpy',
            'pyzmq',
            'cv2',  
      ],
      zip_safe=False)